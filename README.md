#DrawTool3.0 DEMO
移动GIS项目开发中点线面的要素绘制及编辑是最常用的操作，ArcGIS Runtime SDK for iOS 自带AGSSketchLayer类可以帮助用户快速实现要素的绘制，图形编辑。但是在ArcGIS Runtime SDK for Android的版本中并没有提供类似的功能，需要用户自定义扩展实现要素绘制，通过扩展MapOnTouchListener类实现，实现过程相对较复杂。

之前有大神[gispace](http://blog.csdn.net/gispace/article/details/6723459)封装了DrawTools2.0工具类DEMO，实现了简单的要素绘制。并没有对要素绘制及编辑状态做很好的体现，如节点，回退操作等。所以鉴于此，我在DrawTools2.0工具类基础上扩展实现了DrawTools3.0，该版本能够实现基本点线面要素的绘制，精细化展现节点变化信息，支持加点，删点，移点操作。

DrawTools2.0地址：http://blog.csdn.net/gispace/article/details/6723459

##### DrawTool3.0 DEMO 基于ArcGIS Runtime SDK for Android 10.2.8开发

 ![ ](./demo.png)
 
# 使用说明

## 1、初始化DrawTool
```
this.drawTool = new DrawTool(this.mapView);
```
## 2、使用Activity扩展DrawEventListener ，并将当前Activity设置为DrawTool的Listener。
```
//扩展Activity
public class MainActivity extends Activity implements DrawEventListener {
    ……
}

this.drawTool.addEventListener(this);
```
## 3、实现DrawEventListener中的handleDrawEvent方法
```
@Override
public void handleDrawEvent(DrawEvent event) throws TableException, FileNotFoundException {
    // 将画好的图形（已经实例化了Graphic），添加到drawLayer中并刷新显示
    this.graphicsLayer.addGraphic(event.getDrawGraphic());
    this.mapView.setOnTouchListener(mapDefaultOnTouchListener);
}
```
## 4、使用DrawTool工具绘制图形
```
 @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnDrawPoint://绘制点
                drawTool.activate(DrawTool.POINT);
                break;
            case R.id.btnDrawPolyline://绘制线
                drawTool.activate(DrawTool.POLYLINE);
                break;
            case R.id.btnDrawFreePolyline://绘制流状线
                drawTool.activate(DrawTool.FREEHAND_POLYLINE);
                break;
            case R.id.btnDrawPolygon://绘制面
                drawTool.activate(DrawTool.POLYGON);
                break;
            case R.id.btnDrawFreePolygon://绘制流状面
                drawTool.activate(DrawTool.FREEHAND_POLYGON);
                break;
            case R.id.btnDrawCircle://绘制圆
                drawTool.activate(DrawTool.CIRCLE);
                break;
            case R.id.btnDrawEnvlope://绘制矩形
                drawTool.activate(DrawTool.ENVELOPE);
                break;
            case R.id.btnDrawSave://保存
                drawTool.sendDrawEndEvent();
                break;
            case R.id.btnDrawUndo://回退
                drawTool.actionUndo();
                break;
            case R.id.btnDrawDeleteNode://删除节点
                drawTool.actionDelete();
                break;
        }
```
## 5、线面要素分割方法
```
//面要素分割CutPolygonL
public static ArrayList<Polygon> Cut(Polygon gon, Polyline line)
//线要素分割CutPolyline
public static ArrayList<Polyline> Cut(Polyline waitingSplitPolyline, Polyline line)
```

## 博客地址：
[http://www.cnblogs.com/gis-luq/p/5857661.html](http://www.cnblogs.com/gis-luq/p/5857661.html)
